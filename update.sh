#!/bin/bash

set -e

echo "Updating full setup..."

pushd "$(dirname "$0")" >/dev/null

#git submodule init
#git submodule update
git submodule update --init --recursive --remote
#git pull --recurse-submodules # Segfaulted
git pull
git submodule update --init --recursive --remote

function install_symlink() {
	SOURCE="$1"
	TARGET="$2"

	if [ ! -L "${TARGET}" ] && [ -e "${TARGET}" ]; then
		echo "${TARGET} is not a symlink. Creating backup..."
		mv -vf "${TARGET}" "${TARGET}-backup-$(date +%Y%m%d-%H%M%S)"
	fi

	rm -f "${TARGET}"

	SOURCE="$(realpath -e -L "${SOURCE}")"
	TARGET="$(realpath  -L "${TARGET}")"

	ln -v -s -f  "${SOURCE}" "${TARGET}"
}


# GIT
if [ ! -d "${HOME}/stuff/jakes-setup/git-config" ]; then
	git clone git@gitlab.gwdg.de:jakes-setup/git-config.git "${HOME}/stuff/jakes-setup/git-config"
fi
pushd "${HOME}/stuff/jakes-setup/git-config"
git submodule update --init --recursive --remote
git pull
git submodule update --init --recursive --remote
popd
"${HOME}/stuff/jakes-setup/git-config/update.sh"

# BASH
if [ ! -d "${HOME}/stuff/jakes-setup/bash-config" ]; then
	git clone git@gitlab.gwdg.de:jakes-setup/bash-config.git "${HOME}/stuff/jakes-setup/bash-config"
fi
pushd "${HOME}/stuff/jakes-setup/bash-config"
git submodule update --init --recursive --remote
git pull
git submodule update --init --recursive --remote
popd
"${HOME}/stuff/jakes-setup/bash-config/update.sh"

# SSH
if [ ! -d "${HOME}/stuff/jakes-setup/ssh-config" ]; then
	git clone git@gitlab.gwdg.de:jakes-setup/ssh-config.git "${HOME}/stuff/jakes-setup/ssh-config"
fi
pushd "${HOME}/stuff/jakes-setup/ssh-config"
git submodule update --init --recursive --remote
git pull
git submodule update --init --recursive --remote
popd
"${HOME}/stuff/jakes-setup/ssh-config/update.sh"

## VIM
#if [ ! -d "${HOME}/.vim" ]; then
#	git clone git@gitlab.gwdg.de:jakes-setup/vim-config.git "${HOME}/.vim"
#fi
#pushd "${HOME}/.vim" >/dev/null
#git submodule update --init --recursive --remote
#git pull
#git submodule update --init --recursive --remote
#popd >/dev/null
#${HOME}/.vim/update.sh

# Neovim
if [ ! -d "${HOME}/stuff/jakes-setup/neovim-config" ]; then
	git clone git@gitlab.gwdg.de:jakes-setup/neovim-config.git "${HOME}/stuff/jakes-setup/neovim-config"
fi
pushd "${HOME}/stuff/jakes-setup/neovim-config"
git submodule update --init --recursive --remote
git pull
git submodule update --init --recursive --remote
popd
"${HOME}/stuff/jakes-setup/neovim-config/update.sh"

# Sway
if [ ! -d "${HOME}/stuff/jakes-setup/sway-config" ]; then
	git clone git@gitlab.gwdg.de:jakes-setup/sway-config.git "${HOME}/stuff/jakes-setup/sway-config"
fi
pushd "${HOME}/stuff/jakes-setup/sway-config"
git submodule update --init --recursive --remote
git pull
git submodule update --init --recursive --remote
popd
"${HOME}/stuff/jakes-setup/sway-config/update.sh"

# NeoChat
if [ ! -d "${HOME}/stuff/jakes-setup/neochat-config" ]; then
	git clone git@gitlab.gwdg.de:jakes-setup/neochat-config.git "${HOME}/stuff/jakes-setup/neochat-config"
fi
pushd "${HOME}/stuff/jakes-setup/neochat-config"
git submodule update --init --recursive --remote
git pull
git submodule update --init --recursive --remote
popd
"${HOME}/stuff/jakes-setup/neochat-config/update.sh"


popd >/dev/null

echo "Updated full setup."
echo "Please run 'source ~/.profile'"
